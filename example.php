<?php
require_once('src\ValueExtractor.php');

$smsFileName = "smsExample.txt";
$formatFileName = "extractFormat.txt";

if(file_exists($formatFileName))
{
    $formatFile = fopen($formatFileName, "rb");
    $formatString = stream_get_contents($formatFile);
    fclose($formatFile);

    if(file_exists($smsFileName))
    {
        $smsFile = fopen($smsFileName, "rb");
        $smsString = stream_get_contents($smsFile);
        fclose($smsFile);

        $password = new Mask(Mask::PASSWORD, $formatString, $smsString);
        $amount = new Mask(Mask::AMOUNT, $formatString, $smsString);
        $id = new Mask(Mask::ID, $formatString, $smsString);

        echo "Пароль: " . $password . "\n";
        echo "Спишется " . $amount . "\n";
        echo "Перевод на счет " . $id . "\n";

    } else {
        throw new Exception("Sms file doesn't exist");
    }
} else {
    throw new Exception("Format file doesn't exist");
}
