<?php

class Mask {

    const PASSWORD = "#\[password](.*?)\[/password\]#";
    const AMOUNT = "#\[amount](.*?)\[/amount\]#";
    const ID = "#\[id](.*?)\[/id\]#";

    var $mask;
    var $from;
    var $result;
    var $data;
    var $value;

   function __construct($mask, $from, $smsString)
   {
       $this->mask = $mask;
       $this->from = preg_replace('~[\r\n?]+~', '', $from);
       $this->process();

       $this->data = new Data($this->result);
       $this->data->process();

      $this->value = new Value($smsString, $this->data);
      $this->value->process();
   }

   public function getString()
   {
       return $this->mask;
   }

   public function getFrom()
   {
       return $this->from;
   }

   public function getResult()
   {
       return $this->result;
   }

   public function getValue()
   {
       return $this->value;
   }

   private function process()
   {
        if (!preg_match($this->mask, $this->from, $result)) throw new \Exception("Mask not found", 1);
        $this->result = $result[1];
   }

   function __toString()
   {
       return $this->value->getResult();
   }

}

class Data {
    var $data;
    var $before;
    var $after;
    var $line;
    var $length;

   function __construct($data)
   {
       $this->data = $data;
       $this->before = null;
       $this->after = null;
       $this->line = null;
       $this->length = null;
   }

   function hasBefore()
   {
       return !is_null($this->before);
   }

   function hasAfter()
   {
       return !is_null($this->after);
   }

   function hasLine()
   {
       return !is_null($this->line);
   }

   function hasLength()
   {
       return !is_null($this->length);
   }

   function hasType()
   {
       return !is_null($this->type);
   }

   function getBefore()
   {
       return $this->before;
   }

   function getAfter()
   {
       return $this->after;
   }

   function getLine()
   {
       return $this->line;
   }

   function getLength()
   {
       return $this->length;
   }

   function getType()
   {
       return $this->type;
   }

   function process()
   {
       if (preg_match("#\[before](.*?)\[/before\]#", $this->data, $before)){
           $this->before = $before[1];
       }

       if (preg_match("#\[after](.*?)\[/after\]#", $this->data, $after)){
           $this->after = $after[1];
       }

       if (preg_match("#\[line](.*?)\[/line\]#", $this->data, $line)){
           $this->line = (int)$line[1];
       }

       if (preg_match("#\[length](.*?)\[/length\]#", $this->data, $length)){
           $this->length = (int)$length[1];
       }

       if (preg_match("#\[type](.*?)\[/type\]#", $this->data, $type)){
           switch($type[1]){
               case "number":
                    $this->type = 1;
                    break;
                case "string":
                    $this->type = 2;
                    break;
                default:
                    throw new \Exception("Type should be whether number or string", 1);
           }
       }

   }
}



class Value {
    var $smsString;
    var $data;
    var $result;

   function __construct($smsString, $data)
   {
       $this->smsString = $smsString;
       $this->data = $data;
   }

   function getResult()
   {
       return $this->result;
   }

   function process()
   {
       $finalString = $this->smsString;
       if ($this->data->hasLine()){
           $lines = explode(PHP_EOL, $this->smsString);
           $finalString = $lines[$this->data->getLine() - 1];
       }

       if ($this->data->hasBefore()){
           $finalString = explode($this->data->getBefore(), $finalString)[1];
       }


       if ($this->data->hasAfter()){
           $finalString = explode($this->data->getAfter(), $finalString)[0];
       }

       if ($this->data->hasLength() && $this->data->hasType()){
           switch ($this->data->getType()){
               case 1:
                    if (preg_match('/b[0-9]{' . $this->data->getLength() . '}$b/', $finalString, $passwordWithExactLength))
                        $finalString = $passwordWithExactLength;
                break;
               case 2:
                   if (preg_match('/b[a-zA-ZА-Яа-я]{' . $this->data->getLength() . '}$b/', $finalString, $passwordWithExactLength))
                       $finalString = $passwordWithExactLength;
                break;
           }
       }
       $this->result = trim($finalString);
   }
}
